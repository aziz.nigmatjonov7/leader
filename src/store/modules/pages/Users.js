import axios from "../../../plugins/axios";
import { defineStore, storeToRefs } from 'pinia'
import { BASE_URL } from '../../../variables';
import { authStore } from '../../auth';


export const usersStore = defineStore('users', {

  state: () => {
    let authStorage = storeToRefs(authStore());

    return {
      user: authStorage.user,
    }
  },

  getters: {},


  actions: {
    async GET_USERS_LIST (params) {
      return await axios({
          headers: {
            Authorization: this.user.token_type + " " + this.user.access_token,
          }, 
          method: "GET", 
          url: BASE_URL + `api/v1/users?type=user`,
          params     
        })
        .then((e) => {
          return e;
        })
        .catch((error) => {
          return error;
        })
    },
    async CREATE_USER (data) {
      return await axios({
          headers: {
            Authorization: this.user.token_type + " " + this.user.access_token,
          }, 
          method: "POST", 
          url: BASE_URL + `api/v1/users`,
          data     
        })
        .then((e) => {
          return e;
        })
        .catch((error) => {
          return error;
        })
    },
    async UPDATE_USER ({data, id}) {
      return await axios({
        headers: {
          Authorization: this.user.token_type + " " + this.user.access_token,
        }, 
        method: "PUT", 
        url: BASE_URL + `api/v1/users/${id}`,
        data     
      })
      .then((e) => {
        return e;
      })
      .catch((error) => {
        return error;
      })
    },
    async FREEZ_OR_UNFREEZ_USER ({data, id}) {
      return await axios({
        headers: {
          Authorization: this.user.token_type + " " + this.user.access_token,
        }, 
        method: "PUT", 
        url: BASE_URL + `api/v1/users/${id}`,
        data     
      })
      .then((e) => {
        return e;
      })
      .catch((error) => {
        return error;
      })
    },
    async DELETE_USER (id) {
      return await axios({
          headers: {
            Authorization: this.user.token_type + " " + this.user.access_token,
          }, 
          method: "DELETE", 
          url: BASE_URL + `api/v1/users/${id}`,  
        })
        .then((e) => {
          return e;
        })
        .catch((error) => {
          return error;
        })
    },
  },
})