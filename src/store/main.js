import axios from "@/plugins/axios";
import { defineStore, storeToRefs } from 'pinia'
import { BASE_URL } from '@/variables';
import { authStore } from './auth.js';
import { ref } from "vue";
// let user = JSON.parse(localStorage.getItem('user'));

export const mainStore = defineStore('main', {
  state: () => {
    let authStorage = storeToRefs(authStore());
    return {
      current_product_income: null,
      product_categories_list: [],
      income_production: [],
      outcome_production: [],
      storage_production: [],


      sales: [],
      
      
      clients_list: [],
      suppliers_list: [],
      one_client: [],
      
      
      orders_list: [],
      
      
      employees: [],

      perissions: [],

      user: authStorage.user, 

      rules: [
        // {
        //   action: 'manage',
        //   subject: 'all',
        //   // section: 'sidebar'
        // },
        {
          action: ['read', 'update', 'delete'],
          subject: 'sidebar',
        },
      ],


    };
  },
  getters: {
  
  },
  actions: {
    getCurrentProductIncome (payload) {
      this.current_product_income = payload
    },



    // ==================== GET_REQUESTS START ====================
    async GET_PERMISSIONS_LIST(payload) {
      return await axios({
          method: "GET", 
          url: BASE_URL + `api/v1/permissions?role_id=${payload}`,
          headers: { Authorization: this.user.token_type + " " + this.user.access_token,},
        })
        .then((e) => {
          this.perissions = e.data;
          return e.data;
        })
        .catch((error) => {
          return error;
        })
    },
    // ==================== GET_REQUESTS END ====================


    // ==================== PUT_REQUESTS START ====================
    async PUT_PERMISSIONS_LIST(payload) {
      return await axios({
          method: "PUT", 
          url: BASE_URL + `api/v1/permissions/${payload.id}`,
          headers: { Authorization: this.user.token_type + " " + this.user.access_token},
          data: { permissions: payload.permissions }
        })
        .then((e) => {
          return e.data;
        })
        .catch((error) => {
          return error;
        })
    },
    // ==================== PUT_REQUESTS END ====================


























  },
})